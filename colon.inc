%ifndef next
%define next 0
%endif

%macro colon 2
%2: 
dq next
db %1, 0
%define next %2
%endmacro