ASM=nasm
ASMFLAGS=-g -felf64 -o
LD=ld
LDFLAGS=-o

main: main.o lib.o dict.o
	$(LD) $(LDFLAGS) $@ $^
main.o: main.asm
	$(ASM) $(ASMFLAGS) $@ $<
lib.o: lib.asm
	$(ASM) $(ASMFLAGS) $@ $<
dict.o: dict.asm
	$(ASM) $(ASMFLAGS) $@ $<
.PHONY: all clean
all: main
clean:
	rm -rf *.o
