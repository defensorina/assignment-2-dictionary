section .bss
buffer: resb 255
section .data
%include "dict.inc"
%include "words.inc"
%include "lib.inc"

section .rodata
key: db "Введите, пожалуйста, ключ: ", 0
err_key: db "Такой ключ, к сожалению, не найден", `\n`, 0
err_read: db "К сожалению, буффер переполнен", `\n`, 0

section .text
global _start
_start:
mov rdi, key
call print_string
mov rdi, buffer
mov rsi, 255
call read_word          
    cmp rax, 0             
    je .err_read;      
    mov rdi, rax           
    mov rsi, next     
    call find_word          
    cmp rax, 0              
    je .unfound                   
    lea rdi, [rax + 8]             
    push rdi                
    call string_length
    pop rdi
    lea rdi, [rdi + rax + 1]
    call print_string
    call print_newline
    xor rdi, rdi
    call exit
    .err_read:
        mov rdi, err_read
        call print_error
        mov rdi, 1
        call exit
    .unfound:
        mov rdi, err_key
        call print_error
        mov rdi, 1
        call exit
