section .text
%include "lib.inc"

global find_word
find_word:
; первый -ключ, второй -адрес начала 
.begin:
push rsi 
add rsi, 8
call string_equals
pop rsi 
cmp rax, 1
je .found
mov rsi, [rsi]
cmp rsi, 0
je .notfound
jmp .begin
.found:
mov rax, rsi
ret
.notfound:
xor rax, rax
ret

