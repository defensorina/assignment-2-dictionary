%define exit_code 60 

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, exit_code                 
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
     xor rax, rax ; обнуляем rax
    .begin:
        cmp byte[rdi + rax], 0 ; проверям не равна ли длина строки нулю 
        je .end ; если да, то конец
        inc rax ; считаем длину строки 
        jmp .begin
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length          ; вызываем функцию возвращающую длину строки
    pop rdi
    mov rdx, rax                ; передаем значения 
    mov rax, 1                  ; write
    mov rsi, rdi                ; передаем значения строки в rsi
    mov rdi, 1                  ; передаем значение stdout
    syscall
    ret

print_error:
    push rdi
    call string_length          ; вызываем функцию возвращающую длину строки
    pop rdi
    mov rdx, rax                ; передаем значения 
    mov rax, 1                  ; write
    mov rsi, rdi                ; передаем значения строки в rsi
    mov rdi, 2                  ; передаем значение stderr
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; помещаем в стэк код символа 
    mov rax, 1  ; write
    mov rsi, rsp ; передаем ссылку на начало строки 
    mov rdx, 1 ; передаем длину сивола
    mov rdi, 1 ; передаем значение stdout
    syscall
    pop rdi ; очищаем стек 
    ret
    


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; добавляем символ 
    jmp print_char ; выводим  символ
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; в rax записываем число для деления
    mov r8, 10 ; в r8 записываем 10(на что будем делить)
    xor r10, r10 ; очищаем регистр для счетчика 
    .begin:
        xor rdx, rdx ; очищаем rdx(для деления)
        div r8 ; делим на 10
        add rdx, 0x30   ; переводим в ASCII код цифру
        push rdx ; записываем в стек
        inc r10 ; увеличиваем счётчик 
        test rax, rax ; если число закончилось, то выходим из цикла 
        je .begin2
        jmp .begin
    .begin2:
        cmp r10, 0 ; сравниваем счётчик и нуль 
        je .end ; если равны(цифры закончились), то выходим из цикла
        pop rdi ; очищаем стек
        call print_char ; выводим цифру
        dec r10 ; уменьшаем счетчик
        jmp .begin2
    .end:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; проверяем rdi,больше, или меньше нуля, или равно нулю 
    jge .print ; если больше или равно то выводим
    push rdi ;  записываем в стек rdi 
    mov rdi, '-' ; записываем в rdi  минус 
    call print_char ; выводим минус 
    pop rdi ; выводим из стека rdi 
    neg  rdi ;отрицаем число 
    .print:
    jmp print_uint ; выводим число 
    
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov r9, 1 ; будем считать, что строки равны 
    .begin:
        mov r10b, byte[rdi]
        cmp r10b, byte[rsi] ; сравнивем строки
        jne .breakpoint ; если не равны , то  меняем r9 на 0
        cmp byte[rsi], 0 ; если одно равно нулю, то в r9 1
        je .end
        inc rdi ; указатель первой строки увеличивается на 1
        inc rsi ;указатель второй строки увеличивается на 1
        jmp .begin
    .breakpoint:
        mov r9, 0 
    .end:
        mov rax, r9
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 ; очищаем буфер для символа 
    mov rax, 0 ;read
    mov rdi, 0 ; stdin
    mov rsi, rsp ; передаем адрес буфера
    mov rdx, 1 ; передаем длину символа 
    syscall
    pop rax ; прочитанный символ идет в rax 
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r9, r9 ; обнуляем регистр для счётчика
    
    .begin:
        push rdi
        push rsi
        call read_char ; читаем символ строки 
        pop rsi
        pop rdi
        cmp rax, ` ` ; проверка на пробел 
        je .first
        cmp rax, `\t` ; проверка на табуляцию 
        je .first
        cmp rax, `\n` ; проверка на перевод строки 
        je .first
        jmp .symbol
    .symbol: 
        cmp rsi, r9 ; проверка на заполненность буфера 
        je .err ; если заполнен, то err   
        test rax, rax ; проверка на конец строки 
        je .success
        mov [rdi+r9], rax ; записываем символ 
        inc r9 ; увеличиваем счётчик(количество записанных символов)
        jmp .begin
    .first: ; цикл проверки на первый символ 
        test r9, r9 ; проверка на прочтение слова(если равно нулю, то мы просто прошли пробел/табуляцию/перевод строки)
        je .begin ; если равно нулю, то заново
        jmp .success ; если не равно нулю, значит прочли слово 
    .err:
        xor rax, rax ; обнуляем rax
        xor rdx, rdx ; обнуляем rdx
        ret
    .success:
        mov rax, 0 ; дописываем нуль-терминатор
        mov [rdi+r9], rax ; записываем символ 
        mov rax, rdi ; в rax адрес буфера 
        mov rdx, r9 ; в rdx длина слова 
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8 
    mov r9, 10
    xor rax, rax
    xor r10, r10
    .begin:
        mov r10b, byte[rdi] ;записываем в доп регистр символ
        cmp r10b, '0' ; проверка, что это число
        jl .break 
        cmp r10b, '9' ; проверка, что это число
        jg .break
        cmp r10b, 0 ; проверяем на конец чтроки 
        je .break
        sub r10b , 0x30 ; переводим ASCII  в число 
        mul r9 ; умножаем на 10
        add rax, r10 ; прибавляем цифру в акк
        inc r8 ; увеличиваем счётчик 
        inc rdi ; двигаемся по строке 
        jmp .begin
    .break:
        test r8, r8 ; проверка, что это конец слова
        je .err
        mov rdx, r8 ; ддлину слова в rdx
        ret
    .err:
        xor rax, rax ; обнуляем акк
        ret
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        cmp byte[rdi], '+' ; если есть плюс 
        je .plus
        cmp byte[rdi], '-' ; если есть минус
        je .minus
        call parse_uint ; пишем как обячное число 
        ret
    .plus: ; для плюса
        inc rdi ; пропускаем знак
        call parse_uint
        inc rdx ; учитываем знак в длине 
        ret
    .minus: ; для минуса
        inc rdi ; пропускаем знак
        call parse_uint
        neg rax ; отрицаем число 
        inc rdx ; учитываем знак в длине
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor r8, r8
xor r9, r9
    .begin:
    cmp rdx, r8 ; проверяем не заполнен ли буфер 
    je .err
    mov r9, [rdi] ; загружаем символ 
    mov [rsi], r9b
    test r9b, r9b; проверяем на конец слова
    je .end 
    inc rsi ; смещаем буфер
    inc rdi ; смещаем строку
    inc r8 ; увеличиваем длину
    jmp .begin
    .err:
    mov rax, 0
    ret
    .end:
    mov rax, r8
    ret
